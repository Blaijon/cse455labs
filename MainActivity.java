package com.example.chuwi.lab3;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.R.attr.bufferType;
import static android.R.attr.data;
import static android.R.id.input;

public class MainActivity extends AppCompatActivity {
    // declaring view variables
    private EditText mEdit;
    private TextView OutputText;
    private Button copyButton;
    private String  jsonString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Assign the views from the layout file to the corresponding variables
        mEdit = (EditText) findViewById(R.id.editText);
        OutputText = (TextView) findViewById(R.id.textView3);
        copyButton = (Button) findViewById(R.id.button);
        copyButton.setOnClickListener(listener);

    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OutputText.setText(mEdit.getText().toString());
            jsonString = mEdit.getText().toString();
            new sendPostRequest().execute( "http://date.jsontest.com." /*put url */);
        };
    };

    public class sendPostRequest extends AsyncTask<String, Void, String> {

        ﻿

        @Override
        protected String doInBackground(String... params) {
//Now in a background thread. Do all work with the URL here.

            HttpURLConnection connection = null;
            StringBuilder total = new StringBuilder();


            String result = ""; // You will need some String variables

            try {
                URL url = new URL(params[0]/*Access the URL you input above using the params array passed to doInBackground*/);
                connection = (HttpURLConnection) url.openConnection();
                int code = connection.getResponseCode();

                if (code == 200/*Check to make sure you get an “OK” response code*/) {
                    InputStream input = new BufferedInputStream(connection.getInputStream());
                    if (input != null) {
                        BufferedReader bReader = new BufferedReader(new InputStreamReader(input));
                        String data = "";
				/* Append everything from the BufferedReader into a String variable to use later. Do this in the while loop */

                        while ((data = bReader.readLine()) != null/*Not the end of the reader*/){
                            total.append(data);
                            total.append(bReader);/*Do something*/
                        }

                    }
                    input.close();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }

	/*Generate a new JSONObject with the data read in from the BufferedReader. */
            try {
                JSONObject jsonObject = new JSONObject(total.toString()/*String holding data from BufferedReader*/);
                /* Get a String from the JSONObject depending on the key being used. */
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return result ; /*You are returning the result to the onPostExecute method following below. */
        }

        @Override
        protected void onPostExecute(String result) {
/*This is where you will display the String you got from the JSONObject in the text field on the app.*/
            super.onPostExecute(result);
            OutputText.setText(result);
        }


    }
}






